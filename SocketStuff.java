import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

public class SocketStuff extends Thread{

	
	private Socket tcpComms;
	private DataInputStream inputStream;
	private DataOutputStream outputStream;
	
	private List<String> messages= new ArrayList<>();	
	
	
	public boolean startConnection(String IP, int PORT) {
		//DEBUG    	System.out.print("SOCKETSTUFF::startConnection()\r");	
		
		try {
			tcpComms = new Socket(IP, PORT);
			inputStream = new DataInputStream(tcpComms.getInputStream());
			outputStream = new DataOutputStream(tcpComms.getOutputStream());
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
	    	System.out.println("Error: Failed to connect");	
			e.printStackTrace();
			return false;
		}
		
		start();
		
		return true;
	}
	
	
	public void endConnection() {
		//DEBUG    	System.out.print("SOCKETSTUFF::endConnection()\r");	
		
		try {
			inputStream.close();
			outputStream.close();
			tcpComms.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
	    	System.out.print("HELLO\r");	
			e.printStackTrace();
		}
	}
	
	
	public boolean sendCommand (String command) {
		//DEBUG    	System.out.println("SOCKETSTUFF::sendCommand: " + command);	
    	
		try {
			outputStream.writeBytes(command);
			outputStream.flush();
			return true;

		} catch (IOException e) {
			// TODO Auto-generated catch block
			//DEBUG	    	System.out.println("ERROR: Connection Lost");	
			e.printStackTrace();
			return false;
		}
	}
	
	
	public List<String> getMessages(){
		
		return messages;
	}
	
	
	public void clearMessages() {
		//DEBUG	    System.out.println("SOCKETSTUFF::clearMessages: ");	

		messages.clear();
	}
	
	
	public void run() {
		//DEBUG	    System.out.println("SOCKETSTUFF::run: main thread starting");	

	    List<Byte> messageBytes = new ArrayList<Byte>();

	    while (tcpComms.isConnected()) {
	    	
			try {
				
				if (inputStream.available()!=0) {
					
					messageBytes.add((byte) inputStream.read());
					
				} else if (!messageBytes.isEmpty()) {
					
					String inMessage = "";
				    
					//convert to a string
				    for (Byte b : messageBytes) {
				    	int i = b.intValue();
				    	inMessage += (char)(i);
				    }
//DEBUG				    System.out.println(messageBytes.toString());
//DEBUG				    System.out.println(inMessage);
				    
				    messages.add(inMessage);
				 	messageBytes.clear();            
				}
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}				
		}     
	}	
}