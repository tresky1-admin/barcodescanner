import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class BarcodeScanner {
	
	
	private List<SR1000> scanners;

	public static void main(String[] args) {
		
		SR1000 scanner5 = new SR1000("192.168.100.100", 9004);

		
		List<String> barcodeData= new ArrayList<>();
		
		while (true) {
			
			
			
			try {
				
				if (scanner5.isActive()) {
					
					scanner5.startScanning();

					TimeUnit.SECONDS.sleep(2);
					
					scanner5.stopScanning();
					
					TimeUnit.SECONDS.sleep(2);

					scanner5.clearSocket();
					
					TimeUnit.SECONDS.sleep(2);

					barcodeData = scanner5.getMessages();
					

					if (!barcodeData.isEmpty()) {
						System.out.println("MAIN: RECIEVED MESSAGES:");

						for(String x : barcodeData) {
							
							System.out.println(x);
						}
						
					} else {
						System.out.println("MAIN: NO MESSAGES\r");

					}
					
					// has to be called after printing cuz SR1000 only returns a soft-copy
					scanner5.clearMessages();

				}
				


				
				TimeUnit.SECONDS.sleep(2);
				
				
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			
		
		}
		
	}
}
