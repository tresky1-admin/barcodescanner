
import java.util.ArrayList;
import java.util.List;



public class SR1000 {
	
	private String inputCommand = "";
	private String IP;
	private int PORT;
		
	private boolean scannerConnected;
	
	private List<String> messages= new ArrayList<>();
	
	private Thread watchDog;
	private Thread comms;
	
	
	
	public SR1000 (String IP, int PORT) {		
		//DEBUG	    System.out.println("SR1000::SR1000: Constructor");	
	   
		this.IP = IP;
		this.PORT = PORT;
		scannerConnected = false;
		startConnection();
		watchDog = new Thread() {
            
            public void run()
            {
            	//DEBUG		System.out.println("SR1000::watchDog: WatchDog Thread Starting");	

            	while (true) {
            		
                    try {
                    	
                        if (!scannerConnected) {
                        	restartConnection();
                        }
                        
						sleep(500);
						
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
            	}
            }
        };        
	}
	
		
	public void startScanning() {
		System.out.println("SR1000::startScanning: ");					
		inputCommand = "LON\r";
	}
	
	
	public void stopScanning() {
		System.out.println("SR1000::stopScanning: ");	
		inputCommand = "LOFF\r";
	}
	
	
	public void clearSocket() {
		System.out.println("SR1000::clearSocket: ");		
		inputCommand = "BCLR\r";
	}
	
	
	public boolean isActive() {
		
		return scannerConnected;
	}
	
	
	public void setIP(String IP) {
		
		this.IP = IP;
	}
	
	
	public List<String> getMessages(){
		//DEBUG	    System.out.println("SR1000::getMessages: ");	
		return messages;
	}
	
	
	public void clearMessages() {
		//DEBUG	    System.out.println("SR1000::clearMessages: ");	
		messages.clear();
	}
	
	
	private void startConnection() {
		
		comms = new Thread() {
			
			public void run() {
				
				//DEBUG    System.out.println("SR1000::COMMS: main thread starting");	

				SocketStuff tcpComms = new SocketStuff();
				scannerConnected = tcpComms.startConnection(IP, PORT);

		        List<String> temp = new ArrayList<>();
		        
		        while (scannerConnected) {
		            
		        	temp = tcpComms.getMessages();

		        	
		        	if (!temp.isEmpty()) {
		        		
		    			String inMessage = "";
		        		
		        		for(String x : temp) {
		        			
		        			//DEBUG    System.out.println("SR1000::MESSAGE: " + x);	

		        			
		        			if (x.contains("ER,")) {
						    	//the last 2 chars are the error code, need to offset since the very last character is [CR] = 13
						    	inMessage = errorMessage(x.substring(x.length()-3, x.length()-1));
		        			} else if (x.contains("OK")) {
		        				//currently not sure what to do with this. ack that BCLR command worked
		        				inMessage = "";
		        			} else {
		        				
		        				inMessage = x.substring(0, x.length()-1);
		        			}
		        			
						    messages.add(inMessage);
		        			
		        		}
		        		
		        		tcpComms.clearMessages();
		        		
		        	} else if (!inputCommand.isEmpty()) {
		        		
		        		scannerConnected = tcpComms.sendCommand(inputCommand);
		        		inputCommand = "";
		        	}

		        }
		        
		        if(!watchDog.isAlive()) {
		        	watchDog.start();
		        }
		        
		      //DEBUG	System.out.println("SR1000::run: main thread ending");	

			}
		};
		
		comms.start();
	}
	
	
	private void restartConnection() {
		//DEBUG	    System.out.println("SR1000::restartConnection: ");	

		if (!comms.isAlive()) {
			System.out.println("SR1000::restartConnection: COMMS is dead, time to reboot ");	
		    startConnection();
		}
	}
	
	
	private static String errorMessage(String errorCode) {
				
		String errorMessage;
    	
    	switch (errorCode) {
    		case "00":
    			errorMessage = "Undefined command received";
    			break;
    		case "01":
    			errorMessage = "Mismatched command format (Invalid number of parameters)";
    			break;
    		case "02":
    			errorMessage = " The parameter 1 value exceeds the set value";
    			break;
    		case "03":
    			errorMessage = "The parameter 2 value exceeds the set value";
    			break;
    		case "04":
    			errorMessage = "Parameter 2 is not set in HEX (hexadecimal) code";
    			break;
    		case "05":
    			errorMessage = " Parameter 2 set in HEX (hexadecimal) code but exceeds the set value";
    			break;
    		case "10":
    			errorMessage = "There are 2 or more ! marks in the preset data. Preset data is incorrect";
    			break;
    		case "11":
    			errorMessage = "Area specification data is incorrect";
    			break;
    		case "12":
    			errorMessage = "Specified file does not exist";
    			break;
    		case "13":
    			errorMessage = "\"mm\" for the %Tmm-LON,bb command exceeds the setting range.";
    			break;
    		case "14":
    			errorMessage = "Communication cannot be checked with the %Tmm-KEYENCE command.";
    			break;
    		case "20":
    			errorMessage = "This command is not executable in the current status (execution error)";
    			break;
    		case "21":
    			errorMessage = "The buffer has overflowed, so commands cannot be executed";
    			break;
    		case "22":
    			errorMessage = "An error occurred while loading or saving parameters, so commands cannot be executed";
    			break;
    		case "23":
    			errorMessage = "Commands sent from RS-232C cannot be received because AutoID Network Navigator is being connected.";
    			break;
    		case "99":
    			errorMessage = "SR-1000 Series may be faulty. Please contact your nearest KEYENCE sales office";
    			break;
    		default:
    			errorMessage = "No Barcode was detected in scanning phase";
    		break;
    	}
    	
    	//System.out.println("ERROR: " + errorMessage);
    	return "ERROR: " + errorMessage + "\r";
	}
}

